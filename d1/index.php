<?php require_once("./code.php"); ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC D1</title>
</head>
<body>
	<h1>Objects from Variables</h1>
	<p><?php var_dump($buildingObj); ?></p>

	<h1>Objects from Classes</h1>
	<p><?php var_dump($building); ?></p>
	<p><?php var_dump($building2); ?></p>
	<p><?= $building->printName(); ?></p>
	<p><?= $building2->printName(); ?></p>

	<h1>Inheritance</h1>
	<p><?= $condominium->floors; ?></p>
	<p><?= $condominium->address; ?></p>

	<h1>Polymorphism</h1>
	<p><?= $condominium->printName(); ?></p>
</body>
</html>
